package uz.setapp.acount_lock.auth;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.setapp.acount_lock.entity.User;
import uz.setapp.acount_lock.repository.UserRepository;

import java.util.Date;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public static final int MAX_FAILED_ATTEMPTS = 3;
    private static final long LOCK_TIME_DURATION = 24 * 60 * 60 * 1000; // 24 hours

    public void increaseFailedAttempts(User user) {
        int newFailAttempts = user.getFailedAttempt() + 1;
        userRepository.updateFailedAttempts(newFailAttempts, user.getEmail());
    }

    public void lock(User user) {
        userRepository.lockedUser(new Date(), false, user.getEmail());
    }

    public boolean unlockWhenTimeExpired(User user) {
        long lockTimeInMillis = 0;
        if (user.getLockTime() != null){
            lockTimeInMillis = user.getLockTime().getTime();
        }
        long currentTimeInMillis = System.currentTimeMillis();

        if (lockTimeInMillis + LOCK_TIME_DURATION < currentTimeInMillis) {
            userRepository.lockedUser(null, true, user.getEmail());
            userRepository.updateFailedAttempts(0, user.getEmail());
            return true;
        }
        return false;
    }
}
