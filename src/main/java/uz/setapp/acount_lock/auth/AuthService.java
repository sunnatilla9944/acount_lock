package uz.setapp.acount_lock.auth;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.setapp.acount_lock.config.JwtService;
import uz.setapp.acount_lock.entity.Role;
import uz.setapp.acount_lock.entity.User;
import uz.setapp.acount_lock.exception.InputDataExistsException;
import uz.setapp.acount_lock.payload.ApiResult;
import uz.setapp.acount_lock.repository.RoleRepository;
import uz.setapp.acount_lock.repository.UserRepository;
import uz.setapp.acount_lock.utils.AppConstants;



@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final RoleRepository roleRepository;
    private final UserService userService;
    private final JdbcTemplate jdbcTemplate;

    @Transactional
    public void insertWithQuery(User user) {
        jdbcTemplate.update("INSERT INTO users (first_name, last_name, email, password, role_id,failed_attempt, account_non_expired,account_non_locked, credentials_non_expired, enabled) VALUES (?,?,?,?,?,?,?,?,?,?)", new Object[]{user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getRole().getId(), 0, true, true, true, true,});
    }

    public ResponseEntity<?> signUp(SignUpDTO signUpDTO) {
        if (userRepository.findByEmail(signUpDTO.getEmail()).isEmpty()) {
            Role role = roleRepository.findByName(AppConstants.USER).orElseThrow(() -> new InputDataExistsException("Role not found!!!"));
            var user = User.builder()
                    .firstName(signUpDTO.getFirstName())
                    .lastName(signUpDTO.getLastName())
                    .email(signUpDTO.getEmail())
                    .password(passwordEncoder.encode(signUpDTO.getPassword()))
                    .role(role)
                    .build();

            insertWithQuery(user);

            var jwtToken = jwtService.generateToken(user);

            return ResponseEntity.ok(ApiResult.successResponce(jwtToken));
        } else {
            return ResponseEntity.ok(ApiResult.failResponse("Bunday email mavjud", 1011));
        }
    }

    public ResponseEntity<?> signIn(SignInDTO signInDTO) {

        User user = userRepository.findByEmail(signInDTO.getEmail()).orElseThrow(() -> new InputDataExistsException("email not found!!!"));

        if (passwordEncoder.matches(signInDTO.getPassword(), user.getPassword()) && userService.unlockWhenTimeExpired(user)) {
            var jwtToken = jwtService.generateToken(user);
            return ResponseEntity.ok(ApiResult.successResponce(jwtToken));
        } else {
            if (user.isAccountNonLocked()) {
                if (user.getFailedAttempt() < UserService.MAX_FAILED_ATTEMPTS - 1) {
                    userService.increaseFailedAttempts(user);
                } else {
                    userService.lock(user);
                    return ResponseEntity.ok(ApiResult.failResponse("Your account has been locked due to 3 failed attempts."
                            + " It will be unlocked after 24 hours.", 200));
                }
            } else {
                return ResponseEntity.ok(ApiResult.successResponce("Account blocklangan"));
            }
        }
        return ResponseEntity.ok(ApiResult.failResponse("Paro'l noto'gri qaytadan urinib ko'rin", 409));
    }
}

