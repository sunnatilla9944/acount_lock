package uz.setapp.acount_lock.entity.enums;

public enum Peremetion {

    ADD_USER,
    EDIT_USER,
    DELETE_USER,
    VIUW_USERS,

    ADD_ROLE,
    EDIT_ROLE,
    DELETE_ROLE,
    VIUW_ROLES,

    ADD_POST,
    EDIT_POST,
    DELETE_POST,

    ADD_COMMENT,
    EDIT_COMMENT,
    DELETE_COMMENT,
    DELETE_MY_COMMENT
}
