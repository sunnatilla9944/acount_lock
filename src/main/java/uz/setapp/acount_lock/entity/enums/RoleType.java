package uz.setapp.acount_lock.entity.enums;

public enum RoleType {

    ROLE_ADMIN, ROLE_USER, ROLE_CUSTOM

}

