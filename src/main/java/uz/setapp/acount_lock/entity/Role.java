package uz.setapp.acount_lock.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.setapp.acount_lock.entity.enums.Peremetion;
import uz.setapp.acount_lock.entity.template.AbcLongAuditingUserAndTime;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Role extends AbcLongAuditingUserAndTime {

    @Column(unique = true,nullable = false)
    private String name; //  ADMIN  ,  USER ,  CUSTOM

    @Enumerated(value = EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)  //   bu shuni yoziwga yordam beradi yani entity boganlani yigiwga yordam beradi
    private List<Peremetion> peremetions;

    @Column(length = 600)
    private String description;
}
