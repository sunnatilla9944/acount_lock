package uz.setapp.acount_lock.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.setapp.acount_lock.entity.enums.Peremetion;
import uz.setapp.acount_lock.entity.template.AbcLongAuditingUserAndTime;


import java.util.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends AbcLongAuditingUserAndTime implements UserDetails {

    private String lastName;

    private String firstName;

    @Column(nullable = false, unique = true)
    private String email;

    private String password;

    @Column(name = "failed_attempt")
    private int failedAttempt;

    @Column(name = "lock_time")
    private Date lockTime;

    private boolean enabled;

    private boolean accountNonExpired = true;

    private boolean accountNonLocked = true;

    private boolean credentialsNonExpired = true;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Role role;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Peremetion> peremetions = this.role.getPeremetions();
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        for (Peremetion peremetion : peremetions) {
            grantedAuthorities.add(new SimpleGrantedAuthority(peremetion.name()));
        }

        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    public User(String firstName,String lastName, String username, String password, Role role, boolean enabled) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = username;
        this.password = password;
        this.role = role;
        this.enabled = enabled;
    }
}
