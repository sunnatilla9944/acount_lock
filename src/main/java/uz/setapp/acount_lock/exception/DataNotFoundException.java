package uz.setapp.acount_lock.exception;

public class DataNotFoundException extends RuntimeException {

  public DataNotFoundException(String massage){
      super(massage);
  }
}
