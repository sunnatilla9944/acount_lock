package uz.setapp.acount_lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcountLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcountLockApplication.class, args);
    }

}
