package uz.setapp.acount_lock.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.setapp.acount_lock.entity.User;

import java.util.Date;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("UPDATE User u SET u.failedAttempt = ?1 WHERE u.email = ?2")
    @Modifying
    public void updateFailedAttempts(int failAttempts, String email);

    @Query("UPDATE User u SET u.lockTime = ?1, u.accountNonLocked = ?2 WHERE u.email = ?3")
    @Modifying
    public void lockedUser(Date lockTime, Boolean accountNonLocked, String email);

    Optional<User> findByEmail(String email);
}
