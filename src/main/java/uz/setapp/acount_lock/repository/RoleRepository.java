package uz.setapp.acount_lock.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.setapp.acount_lock.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByName(String name);

    boolean existsByName(String name);
}
