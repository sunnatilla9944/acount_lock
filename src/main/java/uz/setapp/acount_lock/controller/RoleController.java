package uz.setapp.acount_lock.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import uz.setapp.acount_lock.aop.CheckPermission;
import uz.setapp.acount_lock.payload.RoleDTO;
import uz.setapp.acount_lock.service.RoleService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @CheckPermission(role = "ADD_ROLE")
    @PostMapping
    public HttpEntity<?> addRole(@Valid @RequestBody RoleDTO roleDTO) {
        return roleService.addRole(roleDTO);
    }

    @PreAuthorize(value = "hasAnyAuthority('EDIT_ROLE')")
    @PutMapping("/{id}")
    public HttpEntity<?> editRole(@PathVariable Long id,
                                  @Valid @RequestBody RoleDTO roleDTO) {
        return roleService.editRole(id, roleDTO);
    }
}
