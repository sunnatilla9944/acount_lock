package uz.setapp.acount_lock.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import uz.setapp.acount_lock.entity.User;
import uz.setapp.acount_lock.exception.InputDataExistsException;

@Component
@Aspect
public class CheckPermissionExecuter {
    @Before(value = "@annotation(checkPermission)")
    public void checkUserPermissionMyMethod(CheckPermission checkPermission){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        boolean exist = false;

        for (GrantedAuthority authority : user.getAuthorities()) {
            if(authority.getAuthority().equals(checkPermission.role())) {
                exist = true;
                break;
            }
        }

        if(!exist){
            throw new InputDataExistsException(checkPermission.role(), "Ruhsat yoq");
        }

//        checkPermission.role()


    }
}

