package uz.setapp.acount_lock.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.setapp.acount_lock.entity.Role;
import uz.setapp.acount_lock.entity.User;
import uz.setapp.acount_lock.entity.enums.Peremetion;
import uz.setapp.acount_lock.repository.RoleRepository;
import uz.setapp.acount_lock.repository.UserRepository;
import uz.setapp.acount_lock.utils.AppConstants;

import java.util.Arrays;

import static uz.setapp.acount_lock.entity.enums.Peremetion.*;


@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    @Value("${spring.sql.init.mode}")
    private String modeType;


    @Override
    public void run(String... args) throws Exception {

        if (modeType.equals("always")) {

            Peremetion[] values = Peremetion.values();

            Role admin = roleRepository.save(new Role(
                    AppConstants.ADMIN,
                    Arrays.asList(values),
                    "admin"
            ));

            Role user = roleRepository.save(new Role(
                    AppConstants.USER,
                    Arrays.asList(ADD_COMMENT, EDIT_COMMENT, DELETE_COMMENT),
                    "user"
            ));

            userRepository.save(new User(
                    "Sunnatilla",
                    "Sunnatillayev",
                    "sunnatilla9944@gmail.com",
                    passwordEncoder.encode("gold"),
                    admin,
                    true

            ));
            System.out.println("Run boldi");
        }
    }
}
