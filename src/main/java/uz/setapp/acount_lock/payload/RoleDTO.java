package uz.setapp.acount_lock.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.setapp.acount_lock.entity.enums.Peremetion;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoleDTO {

    @NotBlank
    private String name;

    private String description;

    @NotEmpty
    private List<Peremetion> peremetions;
}
