package uz.setapp.acount_lock.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.setapp.acount_lock.entity.Role;
import uz.setapp.acount_lock.exception.InputDataExistsException;
import uz.setapp.acount_lock.payload.RoleDTO;
import uz.setapp.acount_lock.repository.RoleRepository;


@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public HttpEntity<?> addRole(RoleDTO roleDTO) {

        if (roleRepository.existsByName(roleDTO.getName())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("bunday role mavjud !!!");
        }

        Role role = new Role(
                roleDTO.getName(),
                roleDTO.getPeremetions(),
                roleDTO.getDescription()
        );

        roleRepository.save(role);

        return ResponseEntity.ok().body("succes role");
    }

    public HttpEntity<?> editRole(Long id, RoleDTO roleDTO) {

        roleRepository.findByName(roleDTO.getName()).orElseThrow(() -> new InputDataExistsException("bunday role mavjud emas !!!"));

        Role role1 = roleRepository.findById(id).orElseThrow(() -> new InputDataExistsException("Bunday id mavjud emas!!!"));


        role1.setName(roleDTO.getName());
        role1.setDescription(roleDTO.getDescription());
        role1.setPeremetions(roleDTO.getPeremetions());

        roleRepository.save(role1);

        return ResponseEntity.ok().body("succes role");
    }
}
